# Generated language files
The API is automatically generated from .proto file. Pick the folder corresponding to the programming language you would like to use for your script. Each folder contains a sample script. Note all these examples follow the documentation in the proto file.

# For Tyto Robotics employees
To change the API, here is the process:
1) modify the .proto definition.
2) go to the go/js/python folders and regenerate the compiled files following the instructions in each folder.
3) make a commit in this repository to update the generated files.
4) copy/paste the generated files in the go/js/python applications.
5) create a tag to match the new version of the Flight Stand Software.
