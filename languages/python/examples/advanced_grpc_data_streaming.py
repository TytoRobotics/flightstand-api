# This example is for advanced users, and is directly interacting with grpc instead of using flightstand.py wrapper
# class. For most users, it is better to refer to the example_data_polling.py example to continuously read sensor data.
# This example shows how to use the WatchSamples rpc call to continuously stream data. It is important to note the core
# will automatically discard samples if the Python execution is blocked. For this reason, if you want to continuously
# stream data while performing other tasks, multithreading is recommended.
import threading
import time
import grpc
import flight_stand_api_v1_pb2 as fs
import flight_stand_api_v1_pb2_grpc as fs_grpc

print(" **** Running example script in Python **** ")

print("Connecting to core...")
channel = grpc.insecure_channel('localhost:50051')
stub = fs_grpc.FlightStandStub(channel)

# Confirming connection to core
GetServerStatusRequest = fs.GetServerStatusRequest()
ServerStatus = stub.GetServerStatus(GetServerStatusRequest)
print("Core connected!")  # If we reach this line without an error thrown, it means the core responded.

# Listing available hardware
print("Listing available hardware:")
ListBoardsResponse = stub.ListBoards(fs.ListBoardsRequest())
for listedBoard in ListBoardsResponse.boards:
    print(listedBoard.name + ": " + listedBoard.display_name)

# We want to work with a known hardware, in the case of this example we work with simulated_1 circuit, so we check if
# it is available.
boardName = "/boards/simulated_1"  # this is the name of the board we want to work with.
board = False
for listedBoard in ListBoardsResponse.boards:
    if listedBoard.name == boardName:
        board = listedBoard

# If the simulated board does not exist, we add it
if not board:
    # Connect a simulated board
    print("No hardware found. Connecting simulated hardware...")
    stub.CreateSimulatedBoard(fs.CreateSimulatedBoardRequest())
    print("Waiting for simulated hardware to connect...")
    time.sleep(4)
    print("Done.")

    # Confirm it has connected
    print("Listing available hardware:")
    ListBoardsResponse = stub.ListBoards(fs.ListBoardsRequest())
    for listedBoard in ListBoardsResponse.boards:
        print(listedBoard.name + ": " + listedBoard.display_name)
        if listedBoard.name == boardName:
            board = True
    if not board:
        raise RuntimeError("Cannot add simulated board")

# Lets list all available inputs (sensors)
ListInputsResponse = stub.ListInputs(fs.ListInputsRequest())
print("Listing available inputs:")
for listedInput in ListInputsResponse.inputs:
    # Print some of the properties an input has. See the .proto for details.
    print(listedInput.name + ": type=" + str(listedInput.input_type) + " signalName=" + listedInput.signal_name)

# We want to focus this example on the thrust sensor
# First, we need to know the ID of the sensor type "thrust". Based on the .proto, we
# can find the enumeration of constants:
#     enum InputType {
#       [...]
#       FORCE_FX = 9;
#       FORCE_FY = 10;
#       FORCE_FZ = 11;
#       [...]
#     }
# Then we need to find the thrust sensor for this simulated hardware and determine the corresponding signal name.
# Each sensor is streaming data as a signal. A signal is a stream of values. For efficiency reasons, we kept the list
# of inputs and the signals as separate elements. Therefore, we need to find the signal name for this specific input.
thrust_signal_name = ""
for listedInput in ListInputsResponse.inputs:
    # According to the .proto each input name is of format: /boards/BOARD-ID/inputs/INPUT-ID
    # We know we want to use the thrust sensor for a specific hardware, so we check the BOARD-ID is correct:
    if boardName + "/" in listedInput.name:
        if listedInput.input_type == fs.FORCE_FZ:
            thrust_signal_name = listedInput.signal_name
            print("Thrust sensor signal name: " + thrust_signal_name)
if not thrust_signal_name:
    raise RuntimeError("Cannot find the required thrust sensor")

# This method is a generator function that yields only once we received a response from the server
evt = threading.Event()


def watch_samples_request_stream():
    while True:
        evt.clear()
        yield fs.WatchSamplesRequest()
        evt.wait(timeout=1)


# Here we print a loop continuously handles the WatchSamples rpc
# rpc WatchSamples(stream WatchSamplesRequest) returns (stream WatchSamplesResponse);
WatchSamplesResponseStream = stub.WatchSamples(watch_samples_request_stream())
for WatchSamplesResponse in WatchSamplesResponseStream:
    evt.set()  # As per the API documentation, we need to acknowledge the response (ping pong communication)
    for sampleGroup in WatchSamplesResponse.sample_groups:
        for sample in sampleGroup.samples:
            if sample.signal_name == thrust_signal_name:
                print("Thrust: " + str(sample.filtered_value) + " N")  # In Newtons
