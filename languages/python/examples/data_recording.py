# This example shows how to interact with the Flight Stand data recording features. To run this example
# script, please connect some hardware (or simulated hardware) to the Flight Stand. Data recording is the only
# way to reach true 1000 Hz sample rate for the sensors that have 1000 Hz sample rate. This is because the flight stand
# hardware sends its data in chunks.

import time

from flightstand import FlightStand

print(" **** Running example script in Python **** ")
core = FlightStand()

if len(core.list_boards()) == 0:
    print("No hardware connected, cannot run this example")
    exit()

# Clear existing test data
core.clear_test()

# Take 8 consecutive data samples on the current test
for _ in range(8):
    core.take_sample()
    time.sleep(1)

# Check number of recorded samples (should be equal to 8)
test_recorder = core.get_test_recorder()
print("Manual samples taken: " + str(test_recorder.manual_samples_count))
print()

print("Continuous recording for 5 seconds...")
# Start continuous recording
test_recorder.is_continuous_recording = True
core.update_test_recorder(test_recorder, ['is_continuous_recording'])
time.sleep(5)

# Stop continuous recording
test_recorder.is_continuous_recording = False
core.update_test_recorder(test_recorder, ['is_continuous_recording'])
print()

# To save a test, a title must be provided
test_recorder.title = "Python demo script test data"
core.update_test_recorder(test_recorder, ['title'])

# Save the test and get the name of the newly saved test. The name can be used to later retrieve the test data in Python
test_name = core.save_test()
print()

# Get the thrust sensor
thrust_sensor = core.find_input_by_type(core.Proto.FORCE_FZ)

# Retrieve the recorded manual samples data
print("Retrieving manual samples data:")
manual_data = core.retrieve_test_data(test_name)
print("Rows of retrieved manual data:", len(manual_data))

# Print the manual samples for the manual data
for i in range(len(manual_data)):
    sample = core.get_input_sample_from_test_data(thrust_sensor, manual_data, i)
    print("Thrust sample (N):", str(sample.sample_time.ToDatetime()), sample.value)
print()

# Retrieve the continuous data.
print("Retrieving continuous recording data")
# Retrieving with a resampling resolution of 1ms will return about 5000 lines of data. This demonstrates the built-in
# pagination mechanism.
continuous_data = core.retrieve_test_data(test_name, is_continuous_recording=True, resample_step_duration=0.001)
print("Rows of retrieved continuous data:", len(continuous_data))
print()


print("\nDone")
exit()
