# This example shows how to connect a simulated hardware and interact with it.
# Just run this script, and you should see some data printed in the console.
# If you run into issues, confirm all steps from the README.md in the "/python" folder were followed.

import time

from flightstand import FlightStand

print(" **** Running example script in Python **** ")
core = FlightStand()

# Connect a simulated board
print("\nConnecting simulated hardware...")
board = core.create_simulated_board()
core.print_hardware_list()

# Print out the current thrust value. The thrust sensor is of type FORCE_FZ. See the .proto for details.
thrust_sensor = core.find_input_by_type(core.Proto.FORCE_FZ)
print("\nThrust value (N):")
thrust_sample = core.get_latest_input_sample(thrust_sensor)
print(thrust_sample.filtered_value)  # Values in the API are S.I, so the thrust is in Newtons.

# Activate the output at 1000. This is done by first modifying the output fields to set a new target value, and then
# the update_output command must be called to send the updated values.
print("\nActivating ESC at 1000")
esc_output = core.find_output_by_type(core.Proto.ESC)
esc_output.output_target.target_value = 1000
esc_output.output_target.active = True
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

# Increase the throttle to 1500
print("\nSetting ESC to 1500")
esc_output.output_target.target_value = 1500
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

# Print the thrust value (it should have increased)
print("\nThrust value (N):")
thrust_sample = core.get_latest_input_sample(thrust_sensor)
print(thrust_sample.filtered_value)  # Values in the API are S.I, so the thrust is in Newtons.

# Restore the throttle to 1000
print("\nSetting ESC to 1000")
esc_output.output_target.target_value = 1000
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

# Turn off the ESC output signal
print("\nTurning off ESC")
esc_output.output_target.active = False
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

print("\nDone.")
exit()
