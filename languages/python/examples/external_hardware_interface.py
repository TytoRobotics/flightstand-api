# This example shows how to interface with custom inputs and outputs with the Flight Stand Software. Note external
# hardware requires a license key to work, you can get a license key for free by writing to support@tytorobotics.com
import random
from flightstand import FlightStand

print(" **** Running example script in Python **** ")

core = FlightStand()

# Define external input definitions
external_input_definitions = [
    # Add a voltage sensor
    core.Proto.ExternalInput(
        input_type=core.Proto.VOLTAGE_HV_INPUT,
        min_system_cutoff=3.2,
        max_system_cutoff=4.2
    ),
    # Add a thrust sensor
    core.Proto.ExternalInput(
        input_type=core.Proto.FORCE_FZ,
        min_system_cutoff=-5,
        max_system_cutoff=5
    ),
    # Using "UNKNOWN_INPUT_TYPE" lets you add any type of sensor.
    # Also note no cutoffs means no limit on the sensor range.
    core.Proto.ExternalInput(
        input_type=core.Proto.UNKNOWN_INPUT_TYPE
    )
]

# Define external output definitions
# There are two types of outputs in the Flight Stand, "ESC" and "SERVO". ESC outputs can be mapped as the primary output
# for a power train and is normally for main motors, while SERVO outputs are secondary, such as servos or any other
# external control.
external_output_definitions = [
    # Example controlled using standard PWM with a step size of 1
    core.Proto.ExternalOutput(
        output_type=core.Proto.ESC,
        min_system_value=1000,
        max_system_value=2000,
        step_size=1
    ),
    # Example controlled as percentage with no step size
    core.Proto.ExternalOutput(
        output_type=core.Proto.SERVO,
        min_system_value=0,
        max_system_value=100
    ),
    # Example controlled as percentage with a step size of 0.5%
    core.Proto.ExternalOutput(
        output_type=core.Proto.SERVO,
        min_system_value=0,
        max_system_value=100,
        step_size=0.5
    ),
    # Example digital binary output
    core.Proto.ExternalOutput(
        output_type=core.Proto.SERVO,
        min_system_value=0,
        max_system_value=1,
        step_size=1
    )
]

# Create the external board, optionally with custom name, model, and serial number.
external_board = core.connect_external_board(external_input_definitions, external_output_definitions,
                                             display_name="Python external_hardware_interface.py example board",
                                             model_number="model_XYZ",
                                             serial_number="1249839")
external_inputs = core.list_inputs(board=external_board)
external_outputs = core.list_outputs(board=external_board)

throttle = 0  # A throttle between 0 and 1, affecting a simulated sensor value.


# Regularly called to read the sensor values from the external hardware and send the values to the FlightStand Software.
def inputs_update():
    samples = []

    # The order of the sensors are the same as specified in the external_input_definitions variable
    voltage_input = external_inputs[0]
    thrust_input = external_inputs[1]
    unknown_input = external_inputs[2]

    # Replace this code to read your real sensor values instead of simulated ones like we do here. Sensor data can come
    # from anywhere, including telemetry from a CANbus ESC, sensors on an Arduino board, IOT or web-based sensors.
    # As long as you provide the Python code to read the latest sensor values, you can use it here.

    # Prepare a new data sample (VOLTAGE)
    voltage_value = 3.7 + random.random() * 0.1  # Simulates sensor noise. Replace this code so it reads real sensors.
    samples.append(core.create_sample(voltage_input, voltage_value))

    # Prepare a new data sample (THRUST)
    # This simulated sensor reacts to the throttle variable. At full throttle, the thrust will exceed the limit of 6
    # and should trigger a safety cutoff.
    thrust_value = 6.0 * throttle + random.random() * 0.2
    samples.append(core.create_sample(thrust_input, thrust_value))

    # Prepare a new data sample (UNKNOWN TYPE)
    unknown_input_value = random.random() * 5.0
    samples.append(core.create_sample(unknown_input, unknown_input_value))

    # Send the sensor samples to the Flight Stand Software
    core.write_external_samples(samples)


# Follows the output target specified by the Flight Stand Software
def outputs_update():
    global throttle

    # Get the value the output should be at (as commanded by the Flight Stand Software).
    real_value, active = core.get_external_output_state(external_outputs[0])

    # Change the output on your real hardware by changing this line of code. In this example, we change a variable
    # which affects the simulated sensor value.
    throttle = (real_value - 1000) / 1000  # Throttle value from 0 to 1


def update():
    inputs_update()
    outputs_update()


# Continuously run an update loop at 10Hz
print("Running continuous loop... Interact with your external hardware from the GUI.")
core.run_at_interval(0.1, update)
