"""
The example consists of two separate files that will run in two separate processes in parallel. Each process connects
a "node" to the Cyphal bus. Follow the instructions in example_1_main.py first.

This application simulates a room (plant) with a resistive heating element and a temperature sensor. The heating
element generates heat proportional to the applied voltage which affects the room temperature. The room also looses
heat through its walls. The temperature of the simulated room is calculated from a crude thermodynamics simulation.
The heater voltage is set externally (subscriber) and the temperature is reported to the UAVCAN network (publisher).

In your application, you should have a real piece of hardware (UAVCAN sensor or ESC) connected to the UAVCAN network
instead of this simulated plant.

1) You will need these environment variables configured, as explained in the other file:
CYPHAL_PATH=.\public_regulated_data_types;
UAVCAN__NODE__ID=43;
UAVCAN__PUB__TEMPERATURE_MEASUREMENT__ID=2346;
UAVCAN__SUB__HEATER_VOLTAGE__ID=2347;
UAVCAN__UDP__IFACE=127.0.0.1;

2) Right-click in this code and select "Run example_1_plant" from the context menu. You should see no errors and some
information printed on the console.

3) Assuming example_1_main is correctly running, you should be able to interact with this heater and read the
temperature from the Flight Stand Software.

"""
import os
import time
import asyncio
import pycyphal

# Import DSDL's after pycyphal import hook is installed
import uavcan.si.unit.voltage
import uavcan.si.sample.temperature
import uavcan.time
from pycyphal.application import make_node, NodeInfo

UPDATE_PERIOD = 0.5
heater_voltage = 0.0


def print_uavcan_environment_variables():
    print("UAVCAN environment variables:")
    for name, value in os.environ.items():
        if name.startswith("UAVCAN") or name.startswith("CYPHAL"):
            print(f"{name}: {value}")
    print()


def update_heater_voltage(msg: uavcan.si.unit.voltage.Scalar_1, _metadata: pycyphal.transport.TransferFrom) -> None:
    global heater_voltage
    heater_voltage = msg.volt


async def main() -> None:
    print_uavcan_environment_variables()

    with make_node(NodeInfo(name="demo.plant")) as node:
        temp_environment = 15 + 273.15  # We assume the outside temperature is 15 degrees Celsius
        temp_plant = temp_environment

        # Set up the ports.
        pub_meas = node.make_publisher(uavcan.si.sample.temperature.Scalar_1, "temperature_measurement")
        pub_meas.priority = pycyphal.transport.Priority.HIGH
        sub_volt = node.make_subscriber(uavcan.si.unit.voltage.Scalar_1, "heater_voltage")
        sub_volt.receive_in_background(update_heater_voltage)

        # Run the main loop forever.
        next_update_at = asyncio.get_running_loop().time()
        while True:
            # Publish latest temperature measurement
            await pub_meas.publish(
                uavcan.si.sample.temperature.Scalar_1(
                    timestamp=uavcan.time.SynchronizedTimestamp_1(microsecond=int(time.time() * 1e6)),
                    kelvin=temp_plant,
                )
            )

            # Sleep until the next iteration.
            next_update_at += UPDATE_PERIOD
            await asyncio.sleep(next_update_at - asyncio.get_running_loop().time())

            # Update the simulation.
            temp_plant += heater_voltage * 0.1 * UPDATE_PERIOD  # Energy input from the heater.
            temp_plant -= (temp_plant - temp_environment) * 0.05 * UPDATE_PERIOD  # Dissipation.
            print("Temperature: " + str(temp_plant) + ", Voltage: " + str(heater_voltage))


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
