This folder contains several Cyphal protocol examples that interact with the Flight Stand Software.

The best way to get started is to follow our video tutorial:

[![How to Connect a CAN ESC to Your Thrust Stand Software](https://img.youtube.com/vi/BiZuCrVQNnU/0.jpg)](https://www.youtube.com/watch?v=BiZuCrVQNnU)


To better understand
the examples, we recommend studying the following external resources:

[Cyphal documentation](https://pycyphal.readthedocs.io/en/latest/index.html): the official Cyphal documentation.

[Cyphal guide](https://forum.opencyphal.org/t/the-cyphal-guide/778): a useful guide to understand Cyphal concepts.

[Cyphal demo](https://pycyphal.readthedocs.io/en/latest/pages/demo.html): the examples here are adapted from this demo. Follow it to fully understand the concepts behind Cyphal.

[Zubax Babel](https://zubax.com/products/babel): to interface Cyphal hardware with the PC, you need a UART-CAN adapter like this one. 

[Yakut](https://github.com/OpenCyphal/yakut): a command-line interface to manually troubleshoot and interact with the Cyphal bus.

[Yukon](https://github.com/OpenCyphal/yukon): a graphical interface fully interoperable with Yakut. Having a GUI makes it easier to visualize the whole Cyphal network.

[ESC/Yakut tutorial](https://telega.zubax.com/tutorials/yakut.html): This Yakut tutorial shows how to interact with a Cyphal ESC. You will need to understand this tutorial to interact with your hardware from Yakut.

**Example 1** is designed to work without any external hardware, by 
creating a simulated Cyphal device and interfacing it with the Flight Stand hardware. Run this example before attemping examples 2 and 3.

**Example 2** shows how to add an external Cyphal airspeed probe and have it appear as a sensor in the Flight Stand Software.

**Example 3** shows how to control and get telemetry data from a Zubax Cyphal ESC enabling its control from the Flight 
Stand Software.

Once the user is familiar with Cyphal and is able to interact with their own hardware, using Yukon or Yakut, those
examples should be simple to adapt to any Cyphal hardware.

If you run into issues, try these steps in order:

1) Are you able to run the example code [external_hardware_interface.py](./../external_hardware_interface.py) successfully? If not, it may be a licensing issue.
2) Can you communicate with your Cyphal hardware from your PC? This can be done using the [Zubax Babel](https://zubax.com/products/babel) USB-CAN dongle along with the external software [Yakut](https://github.com/OpenCyphal/yakut) or [Yukon](https://github.com/OpenCyphal/yukon). Read your hardware's documentation. Examples 2 and 3 rely on external Cyphal hardware to work.
3) Is your Python environment setup properly? Make sure Example 1 can run. Example 1 is a simplified example based on the [Cyphal demo](https://pycyphal.readthedocs.io/en/latest/pages/demo.html). The comments in the example contain setup instructions, did you follow them?
4) If all 3 points above are working, you should be able to modify example 2 or 3 to suit your own external Cyphal hardware. The main takeaway is you need to configure the registers as explained in the [Cyphal demo](https://pycyphal.readthedocs.io/en/latest/pages/demo.html) to match your specific hardware, and you need to create matching external inputs/outputs for the Flight Stand Software.
5) Double check all COM ports are available and not being used by external software such as Yukon.
6) Get help. If you can't see your Cyphal hardware from Yukon, please contact the hardware manufacturer's support. If you are able to interact with your hardware using Yukon, but cannot get to interface with the Flight Stand Software, please contact Tyto Robotics support.