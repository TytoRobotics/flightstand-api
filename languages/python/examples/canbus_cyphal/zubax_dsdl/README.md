IMPORTANT, DO NOT MODIFY THE CONTENT OF THIS FOLDER.

This folder was cloned from:
https://github.com/Zubax/zubax_dsdl

It was not included as a git submodule in this project because it keeps the instructions for our examples simpler, but you should follow the official installation instructions of Cyphal for your project.