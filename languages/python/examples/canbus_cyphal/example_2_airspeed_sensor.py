"""

This example shows how to interface a real Cyphal hardware sensor communicating through the UAVCAN network interface.
In this particular example, we interface with an airspeed sensor, consisting of a pressure and temperature sensor.

See the picture ./example_2_hardware.png. In order of connection, we have the following third-party hardware connected:

USB cable:
https://shop.zubax.com/collections/cables-connectors/products/micro-usb-cable-type-b-double-shielded

Zubax Babel USB<->CAN adapter:
https://shop.zubax.com/collections/development-tools/products/zubax-babel

JST 4-pin 0.17m cable that came with the airspeed sensor:
https://shop.zubax.com/products/raccoonlab-airspeednode-v2

The airspeed sensor:
https://shop.zubax.com/products/raccoonlab-airspeednode-v2

UAVCAN termination resistor:
https://shop.zubax.com/collections/cables-connectors/products/uavcan-micro-termination-plug

Follow these instructions to run the example:

1) To ensure correct software setup, ensure example_1.py is works before trying to run this example.

2) Using the same method explained in example_1, set the environment variables as follows, but update the COM# to match
the COM port used by your Zubax Babel:
CYPHAL_PATH=.\public_regulated_data_types;
UAVCAN__CAN__IFACE=slcan:COM4;
UAVCAN__CAN__MTU=8;
UAVCAN__NODE__ID=42;
UAVCAN__CAN__BITRATE=1000000 1000000;
UAVCAN__SUB__AIRSPEED_TEMPERATURE__ID=2602;
UAVCAN__SUB__AIRSPEED_PRESSURE__ID=2600;
UAVCAN__SUB__CIRCUIT_STATUS_5V__ID=3600;
UAVCAN__SUB__CIRCUIT_STATUS_VIN__ID=3602;
UAVCAN__SUB__CIRCUIT_STATUS_TEMPERATURE__ID=3601;

3) Connect all the hardware on your PC, and ensure the hardware is operational using external tools such as Yukon. Don't
forget to close Yukon or any other external tools using the same COM interface.

4) Ensure all the port ID's have been configured on the hardware to match the environment variables listed above.

5) Run the Flight Stand Software.

6) Right-click in this code and select "Run example_2_airspeed_sensor" from the context menu. You should see no errors
and some information printed on the console.

7) Using the Flight Stand Software, you should see the real-time readings from the airspeed sensor.

"""

import os
import asyncio
import pycyphal
from flightstand import FlightStand

# DSDL files are automatically compiled by pycyphal import hook from sources pointed by CYPHAL_PATH env variable.
import pycyphal.application  # This module requires the root namespace "uavcan" to be transcompiled.

# Import namespaces we're planning to use.
import uavcan.node  # noqa
import uavcan.si.sample.temperature  # noqa
import uavcan.si.sample.pressure  # noqa
import uavcan.si.sample.voltage  # noqa
import uavcan.si.unit.temperature  # noqa
import uavcan.si.unit.voltage  # noqa
import uavcan.si.unit.pressure  # noqa


def print_uavcan_environment_variables():
    print("UAVCAN environment variables:")
    for name, value in os.environ.items():
        if name.startswith("UAVCAN") or name.startswith("CYPHAL"):
            print(f"{name}: {value}")
    print()


# Function to handle measurements from UAVCAN and forward them to the Flight Stand Software
def handle_measurement(fs, fs_input, node, port_name, unit_type, value_field):
    def received_new_value(msg, _metadata):
        # Send the new measurement to the Flight Stand Software
        measurement_value = getattr(msg, value_field)
        sample = fs.create_sample(fs_input, measurement_value)
        fs.write_external_samples([sample])

    # To receive data from the UAVCAN network, create a subscriber.
    subscriber = node.make_subscriber(unit_type, port_name)
    subscriber.receive_in_background(received_new_value)


def configure_flight_stand():
    # Define external inputs and outputs for the Flight Stand Software. See the external_hardware_interface.py for
    # more details about using this feature, such as setting limits for cutoffs.
    fs = FlightStand()
    external_input_definitions = [
        fs.Proto.ExternalInput(input_type=fs.Proto.TEMPERATURE_IR),  # airspeed_temperature
        fs.Proto.ExternalInput(input_type=fs.Proto.UNKNOWN_INPUT_TYPE),  # airspeed_pressure
        fs.Proto.ExternalInput(input_type=fs.Proto.TEMPERATURE_IR),  # circuit_status_temperature
        fs.Proto.ExternalInput(input_type=fs.Proto.VOLTAGE_GENERAL_ANALOG_INPUT),  # circuit_status_5v
        fs.Proto.ExternalInput(input_type=fs.Proto.VOLTAGE_GENERAL_ANALOG_INPUT),  # circuit_status_vin
    ]
    external_output_definitions = []

    # Create the external board
    external_board = fs.connect_external_board(external_input_definitions, external_output_definitions,
                                               display_name="RaccoonLab AirspeedNODE v2",
                                               model_number="RL-AIR")
    external_inputs = fs.list_inputs(board=external_board)

    # Map these to powertrain 1, so they can be seen in plots and controlled from the manual control tab
    # This is optional because it can also be done by hand from within the user interface of the Flight Stand Software.
    powertrain = fs.list_powertrains()[0]
    for i in range(5):
        powertrain.extra_input_names.append(external_inputs[i].name)
    fs.update_powertrain(powertrain, ["extra_input_names"])

    # You can configure aliases to help identify the external hardware within the Flight Stand Software.
    # This is optional because it can also be done by hand from within the user interface of the Flight Stand Software.
    # Note aliases are only displayed when mapped to a powertrain extra mapping.
    external_inputs[0].alias = "Airspeed temperature"
    external_inputs[1].alias = "Airspeed pressure (Pa)"
    external_inputs[2].alias = "Airspeed circuit temperature"
    external_inputs[3].alias = "Airspeed circuit 5V"
    external_inputs[4].alias = "Airspeed circuit Vin"
    for i in range(5):
        fs.update_input(external_inputs[i], ["alias"])

    return fs, external_inputs


def run_uavcan_measurement_handlers(fs, external_inputs, uavcan_node):
    # Receive measurements from the UAVCAN network and forward them to the Flight Stand Software
    handle_measurement(fs, external_inputs[0], uavcan_node, "airspeed_temperature", uavcan.si.sample.temperature.Scalar_1, "kelvin")
    handle_measurement(fs, external_inputs[1], uavcan_node, "airspeed_pressure", uavcan.si.sample.pressure.Scalar_1, "pascal")
    handle_measurement(fs, external_inputs[2], uavcan_node, "circuit_status_temperature", uavcan.si.sample.temperature.Scalar_1, "kelvin")
    handle_measurement(fs, external_inputs[3], uavcan_node, "circuit_status_5v", uavcan.si.sample.voltage.Scalar_1, "volt")
    handle_measurement(fs, external_inputs[4], uavcan_node, "circuit_status_vin", uavcan.si.sample.voltage.Scalar_1, "volt")


async def main():
    # Helps with troubleshooting
    print_uavcan_environment_variables()

    # Configure the FlightStand Software
    fs, external_inputs = configure_flight_stand()

    # Start the UAVCAN network connection
    with pycyphal.application.make_node(pycyphal.application.NodeInfo(name="demo.main")) as uavcan_node:

        # Receive measurements from the UAVCAN network and forward them to the Flight Stand Software
        run_uavcan_measurement_handlers(fs, external_inputs, uavcan_node)

        # Continuously run a loop to prevent this program from closing
        print("Running. See the external board created in the Flight Stand Software.")
        while True:
            await asyncio.sleep(0.1)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
