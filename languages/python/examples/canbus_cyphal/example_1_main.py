"""

This example is inspired from the official Cyphal Demo code; however, it has been simplified to demonstrate a
minimalistic way of interfacing the Flight Stand Software with Cyphal nodes. Please note that Cyphal has many
features not covered in this example. For a more comprehensive understanding, you can refer to the official
documentation at: https://pycyphal.readthedocs.io/en/stable/pages/demo.html

The example consists of two separate files that will run in two separate terminals. Each process connects
a node to the Cyphal bus. This application acts like an adapter between the UAVCAN network and the Flight Stand
Software.

Follow the instructions in this file first.

1) This example requires additional python libraries installed. From the PyCharm terminal run the command:
py -m pip install 'pycyphal[transport-can-pythoncan,transport-serial,transport-udp]'

2) We need to set the paths. There are several ways to achieve the same result. In Pycharm, you can right-click in
this code and select "Modify Run Configuration" from the context menu. Next copy/paste the following lines into the run
configuration "Environment variables" field:
CYPHAL_PATH=.\public_regulated_data_types;
UAVCAN__NODE__ID=42;
UAVCAN__UDP__IFACE=127.0.0.1;
UAVCAN__SUB__TEMPERATURE_MEASUREMENT__ID=2346;
UAVCAN__PUB__HEATER_VOLTAGE__ID=2347;

3) Run the Flight Stand Software.

4) Right-click in this code and select "Run example_1_main" from the context menu. You should see no errors and some
information printed on the console.

5) Open example_1_plant.py and continue following the instructions from that file.

6) Using the Flight Stand Software, you should see a temperature sensor and a voltage output control available.

"""

import os
import asyncio
import pycyphal
from flightstand import FlightStand

# DSDL files are automatically compiled by pycyphal import hook from sources pointed by CYPHAL_PATH env variable.
import pycyphal.application  # This module requires the root namespace "uavcan" to be transcompiled.

# Import namespaces we're planning to use.
import uavcan.node  # noqa
import uavcan.si.sample.temperature  # noqa
import uavcan.si.unit.temperature  # noqa
import uavcan.si.unit.voltage  # noqa


def print_uavcan_environment_variables():
    print("UAVCAN environment variables:")
    for name, value in os.environ.items():
        if name.startswith("UAVCAN") or name.startswith("CYPHAL"):
            print(f"{name}: {value}")
    print()


# Handle temperature measurements
def handle_temperature(fs: FlightStand, fs_input: FlightStand.Proto.Input, node: pycyphal.application.Node):
    def received_new_temperature(msg: uavcan.si.unit.temperature.Scalar_1, _metadata: pycyphal.transport.TransferFrom):
        # Send the new temperature measurement to the Flight Stand Software
        sample = fs.create_sample(fs_input, msg.kelvin)
        fs.write_external_samples([sample])

    # To receive data from the UAVCAN network you have to create a subscriber.
    sub_t_pv = node.make_subscriber(uavcan.si.sample.temperature.Scalar_1, "temperature_measurement")
    sub_t_pv.receive_in_background(received_new_temperature)


async def handle_voltage_output(fs: FlightStand, fs_output: FlightStand.Proto.Output,
                                pub: pycyphal.presentation.Publisher):
    # Get the value the output should be at (as commanded by the Flight Stand Software).
    voltage_output, _ = fs.get_external_output_state(fs_output)

    # Send it to the UAVCAN network
    await pub.publish(uavcan.si.unit.voltage.Scalar_1(voltage_output))


async def main():
    # Helps with troubleshooting
    print_uavcan_environment_variables()

    # The FlightStand class is the bridge between the application and the Flight Stand Software.
    fs = FlightStand()

    # Define external inputs and outputs for the Flight Stand Software. See the external_hardware_interface.py for
    # more details about using this feature.
    external_input_definitions = [
        # Add a temperature sensor
        fs.Proto.ExternalInput(
            input_type=fs.Proto.TEMPERATURE_IR
        )
    ]
    external_output_definitions = [
        # Add a control for the heater voltage
        fs.Proto.ExternalOutput(
            output_type=fs.Proto.SERVO,
            min_system_value=0,
            max_system_value=50  # We assume the heater supports a voltage between 0 and 50 V.
        )
    ]

    # Create the external board
    external_board = fs.connect_external_board(external_input_definitions,
                                               external_output_definitions,
                                               display_name="example_1 simulated plant")
    external_inputs = fs.list_inputs(board=external_board)
    external_outputs = fs.list_outputs(board=external_board)

    # Map these to powertrain 1, so they can be seen in plots and controlled from the manual control tab
    # This is optional because it can also be done by hand from within the user interface of the Flight Stand Software.
    powertrain = fs.list_powertrains()[0]
    powertrain.extra_input_names.append(external_inputs[0].name)
    powertrain.extra_output_names.append(external_outputs[0].name)
    fs.update_powertrain(powertrain, ["extra_input_names", "extra_output_names"])

    # You can configure aliases to help identify the external hardware within the Flight Stand Software.
    # This is optional because it can also be done by hand from within the user interface of the Flight Stand Software.
    # Note aliases are only displayed when mapped to a powertrain extra mapping.
    external_outputs[0].alias = "Heater voltage"
    external_inputs[0].alias = "Room temperature"
    fs.update_output(external_outputs[0], ["alias"])
    fs.update_input(external_inputs[0], ["alias"])

    # Start the UAVCAN network connection
    # The Node class is the bridge between the application and the UAVCAN network.
    with pycyphal.application.make_node(pycyphal.application.NodeInfo(name="demo.main")) as uavcan_node:
        # Receive any temperature updates from the UAVCAN network and forward them to the Flight Stand Software
        handle_temperature(fs, external_inputs[0], uavcan_node)

        # To send data to the UAVCAN network, you create a publisher.
        pub_v_cmd = uavcan_node.make_publisher(uavcan.si.unit.voltage.Scalar_1, "heater_voltage")

        # Continuously run a loop to receive control updates from the Flight Stand Software
        print("Running. See the external board created in the Flight Stand Software.")
        while True:
            await asyncio.sleep(0.5)  # This limits the outputs update rate
            await handle_voltage_output(fs, external_outputs[0], pub_v_cmd)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
