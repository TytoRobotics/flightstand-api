"""
This example demonstrates continuous data reading and throttle adjustment to achieve a constant thrust. The control
response is calculated using a closed-loop PID controller. To visualize the thrust response, the target thrust is
alternated between half and full thrust. Make sure the hardware is connected and the output for control is configured
and activated from the manual control tab before running the script. Additionally, configure safety cutoffs specific to
your application.
"""

import time
from flightstand import FlightStand

# The PID controller library needs to work with arbitrary timestamps, since the sensors are sampled in the hardware and
# using the computer's timestamp would yield incorrect results. Also, it needs to accept varying delta timestamps as
# some sensors don't have a fixed sample rate, for example the rotation speed sensor.
# Don't forget to install this library before running this example:
# pip install simple-pid
from simple_pid import PID

""" USER PARAMETERS """

# Set the signal names of the input and output. To find the input and output names, you can run this script once as
# it will  print out all the inputs and outputs found. Refer to the API documentation for the signal type constants (
# search in the .proto file for "enum InputType"). For example a rotation speed sensor is input_type=15.
input_name = "/boards/simulated_1/inputs/1"  # this is the name of the input to control
output_name = "/boards/simulated_1/outputs/1"  # this is the name of the control output

"""
PID terms for the PID controller. These coefficients need to be tuned by the user and are specific to each application.
Tuning PID coefficients is a crucial step to achieve desired control performance. Here are some considerations:

- Tuning Guidance: Refer to resources like [PID Tuning Guide](https://realpars.com/pid-tuning/) for pointers on PID 
tuning. However, keep in mind that there is no one-size-fits-all approach, and results can vary based on different 
factors.

- Factors Affecting PID Coefficients:
  PID coefficients can be influenced by various factors, including:
  - Voltage: The power supply voltage can affect the system's response.
  - Propeller Diameter/Mass: Propeller characteristics can impact the control behavior.
  - Motor Size: The size and specifications of the motor play a role in determining the optimal coefficients.
  - ESC Settings: ESC configuration settings can affect the control response.
  - Type of Input: The type of input being controlled (thrust, RPM, power, temperature) can influence the coefficients.
  - PID update rate: The sample rate can also affect the stability and performance of the control.
  - Output Settings: The output configuration (PWM, DShot, range, rate limiter, etc.) can impact the coefficients.
  - User Application Requirements: Desired control characteristics (fast reaction, no overshoot, etc.) affect the ideal
    coefficients.

- Examples of PID Terms:
  Here are some examples of PID terms used in different scenarios:
  - Simulated Hardware (Constant Thrust): 2000, 1500, 0
  - Simulated Hardware (Constant Thrust with Output Rate Limit of 200): 2000, 500, 0
  - Simulated Hardware (Constant Rotation Speed): 2, 5, 0
  - Real Hardware (Mejzlik 48 x 16,4 Propeller, Xoar 180-35 34kv Motor, Xoar Pulse P200 ESC)
    - Constant Thrust with Rate Limiter of 200us/s at 80V: 6, 5.5, 0.5
    - Constant Rotation Speed with Rate Limiter of 200us/s at 80V: 2, 5, 0.3
"""
Kp = 2000.0  # Proportional terms
Ki = 1500.0  # Integral term
Kd = 0.0  # Derivative term

# This example performs a square wave pattern alternating between 50% of the target_value and 100% of the target_value.
# You can set the period for this pattern in seconds.
pattern_period = 10.0
target_value = 1.0  # The value the input signal should reach

""" SCRIPT """

print(" **** Running example PID script in Python **** ")
core = FlightStand()

controller_output = core.get_output(output_name)
controller_input = core.get_input(input_name)
if not controller_output or not controller_input:
    print("Input or output not found.")
    exit()


def set_throttle(value):
    controller_output.output_target.target_value = value
    core.update_output(controller_output, ['output_target'])


# Setup the PID controller
sample_time = None
target_changed = time.time()
limits = (controller_output.min_user_value, controller_output.max_user_value)
pid = PID(Kp, Ki, Kd, output_limits=limits, sample_time=None)


def update():
    global target_changed, sample_time

    # Calculate the set point to have a square wave pattern with a low point half of the target thrust
    target = target_value
    t = time.time()
    if t - target_changed > pattern_period / 2:
        target = 0.5 * target
    if t - target_changed >= pattern_period:
        target_changed += pattern_period
    pid.setpoint = target

    # Get the latest available sample.
    input_sample = core.get_latest_input_sample(controller_input)

    # Listing samples may repeat the same samples as the rpc returns the latest values available, so we only
    # update the PID controller when the sample time has changed.
    new_sample_time = input_sample.sample_time.ToDatetime()
    if sample_time is None or new_sample_time != sample_time:
        if sample_time is not None:
            dt = (new_sample_time - sample_time).total_seconds()
            val = input_sample.filtered_value

            # Update the PID controller with the new sample value, to obtain the updated control output
            output = pid(val, dt)

            # Apply the output throttle
            set_throttle(output)

            print({
                "Delta time (s)": dt,
                "Target input": pid.setpoint,
                "Input": val,
                "Output": output,
            })
        sample_time = new_sample_time


# We run a continuous loop that reads the latest thrust sensor value and sets the output to achieve a target thrust
core.run_at_interval(0.001, update)
