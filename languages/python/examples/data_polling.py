# This example shows how to use the ListSamples rpc in a loop to continuously read data. The maximum sample rate will
# depend on the network connection speed but also the update rate is limited because the hardware sends its readings
# in chunks, so it is not possible to achieve 1000 Hz closed loop control.
from flightstand import FlightStand

print(" **** Running example script in Python **** ")
core = FlightStand()

# This is the name of the board we want to work with. Use the previous command which prints the name of all available
# boards to determine the name of your real hardware to interact with.
board_name = "/boards/simulated_1"
board = core.get_board(board_name)

# If the simulated board does not exist, we add it
if not board:
    # Connect a simulated board
    print("\nConnecting simulated hardware...")
    core.create_simulated_board()
    core.print_hardware_list()

    # Get the newly connected board
    board = core.get_board(board_name)

# We want to continuously read the thrust sensor value
thrust_sensor = core.find_input_by_type(core.Proto.FORCE_FZ)

# We run a continuous loop that reads the latest thrust sensor value. You can use the manual control tab in the GUI
# to adjust the throttle and affect the thrust value.
sample_time = None
while True:
    # Get the latest available sample.
    thrust_sample = core.get_latest_input_sample(thrust_sensor)
    new_sample_time = thrust_sample.sample_time.ToDatetime()
    # To calculate the sample rate for the thrust sensor, we need to check if the timestamp has been updated
    # Note, even if the hardware can record data at 1000 Hz, the API will not have a real-time update rate of 1000 Hz
    # because of performance optimizations. This means to get a true 1000 Hz sample rate, you need to record the data,
    # and then retrieve the recorded test data from the recorded segment.
    if sample_time is None or new_sample_time != sample_time:
        if sample_time is not None:
            dt = new_sample_time - sample_time
            hz = 1.0/dt.total_seconds()
            print("Sample rate: " + str(int(hz)) + " Hz")
        sample_time = new_sample_time
        print("Thrust: " + str(thrust_sample.filtered_value) + " N")  # In Newtons
