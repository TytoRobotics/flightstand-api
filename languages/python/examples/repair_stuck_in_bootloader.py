# This code was written as a way to restore a corrupted firmware state. The error that typically shows is the unit is
# stuck in bootloader.
from flightstand import FlightStand

print(" **** Firmware repair script **** ")
core = FlightStand()

ListBoardsResponse = core.stub.ListBoards(core.Proto.ListBoardsRequest())
for board in ListBoardsResponse.boards:
    print(board.name)
print()

FlashBoardFirmwareRequest = core.Proto.FlashBoardFirmwareRequest()
FlashBoardFirmwareRequest.board_name = input("Type board name to repair (ie '/boards/COM4'):")
FlashBoardFirmwareRequest.dev_filename = ""
FlashBoardFirmwareRequest.dev_flash_protocol = 0
FlashBoardFirmwareRequestResponse = core.stub.FlashBoardFirmware(FlashBoardFirmwareRequest)

print("\nDone.")
exit()
