# This example shows how to connect a simulated hardware and use the propeller balancing feature. In this example
# the simulated hardware starts with a virtual imbalance, and we run the balancing steps to get it balanced.
# In your scenario, you should replace the simulated circuit with your own hardware.
import time

from examples.balancing.utils import show_balancing_report, format_weights
from flightstand import FlightStand

print(" **** Running example propeller balancing script with simulated hardware **** ")
core = FlightStand()

# Connect a simulated board if none already connected
print("\nConnecting simulated hardware...")
board = core.create_simulated_board()
core.print_hardware_list()

# Activate the output at 1000. This is done by first modifying the output fields to set a new target value, and then
# the update_output command must be called to send the updated values.
print("\nActivating ESC at 1000")
output_name = board.name + "/outputs/1"
esc_output = core.get_output(output_name)
esc_output.output_target.target_value = 1000
esc_output.output_target.active = True
core.update_output(esc_output, ['output_target'])
time.sleep(0.2)

# Increase the throttle to 1500
print("\nSetting ESC to 1500")
esc_output.output_target.target_value = 1500
core.update_output(esc_output, ['output_target'])
time.sleep(0.5)

# Do an initial run in the balancing session to get the initial unbalance (simulated hardware has a random unbalance
# when created)
rotor_mass_kg = 0.02  # Important to correctly calculate the correction weights
radius_m = 0.04  # So we can calculate the weight
core.create_balancing_session(params=core.Proto.BalancingSession(
    title="Demo balance session on 3-bladed simulated hardware",  # Optional
    operator_display_name="My name",  # Optional
    motor_display_name="Motor 1",  # Optional
    propeller_display_name="Propeller 1",  # Optional
    rotor_mass=rotor_mass_kg,
    operating_speed_rad_s=1091,  # In our demo, we go to ESC value 1500 which gives a repeatable rotation speed.
    quality_grade_g=6.3,  # Optional. 6.3 is common in the industry for propellers (ISO 1940 standard)
    correction_radius_m=radius_m,
    rotation_sensor_name=board.name + "/inputs/5",
    vibration_sensor_name=board.name + "/inputs/8",
    control_output_name=output_name,
    blades_count=3
))
core.execute_balance_run(False, None)


# Retrieve the data to know the trial weight to put
session = core.get_balancing_session()
# print_session(session)
initial_run = session.runs[0]

trial_weights = initial_run.next_corrections
print("Trial weights: " + format_weights(trial_weights, radius_m))

# Normally done by user, but here we simulate adding a trial weight
print(f"Putting a simulated trial weight on the simulated hardware")
sim_settings = core.Proto.UpdateSimulatedBoardSettingsRequest(board_name=board.name)
sim_settings.settings.correction_weights.extend(trial_weights)
core.stub.UpdateSimulatedBoardSettings(sim_settings)
time.sleep(0.5)  # Gives time for the data samples to update

# Confirm the simulated hardware has correction weights applied
sim_settings = core.stub.GetSimulatedBoardSettings(core.Proto.GetSimulatedBoardSettingsRequest(board_name=board.name))
print("Confirmed correction weights applied:", sim_settings)
print("")

# Do the trial run
print("Executing trial run")
core.execute_balance_run(False, trial_weights)

# Retrieve the data to know the correction weight to put
session = core.get_balancing_session()
# print_session(session)
trial_run = session.runs[1]
correction_weights = trial_run.next_corrections
print("Correction weights: " + format_weights(correction_weights, radius_m))

# Normally done by user, apply the correction weight to achieve a balanced rotor
sim_settings = core.Proto.UpdateSimulatedBoardSettingsRequest(board_name=board.name)
sim_settings.settings.correction_weights.extend(correction_weights)
core.stub.UpdateSimulatedBoardSettings(sim_settings)
time.sleep(0.5)  # Gives time for the data samples to update

# Do the correction run (will confirm balance is good)
print("Executing correction run")
core.execute_balance_run(True, correction_weights)

# Retrieve the data to print the report
show_balancing_report(core)

# Restore the throttle to 1000
print("\nSetting ESC to 1000")
esc_output.output_target.target_value = 1000
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

# Turn off the ESC output signal
print("\nTurning off ESC")
esc_output.output_target.active = False
core.update_output(esc_output, ['output_target'])
time.sleep(1.0)  # Give time for the output change to be applied

# Delete the balance run
core.delete_balance_run(session.name)

print("\nDone.")
exit()
