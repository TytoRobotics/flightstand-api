import math
from datetime import timedelta

import numpy as np
from matplotlib import pyplot as plt

from flightstand import FlightStand


def input_float(prompt):
    val = input(prompt)
    if val == "":
        return 0.0
    return float(val)


# Calculates datapoints for superposing a sine wave function on top of the data, to show correct curvefitting
def simulate_sine_result(run: FlightStand.Proto.BalanceRun, rotation_speed: list[FlightStand.Proto.DataPoint]):
    # Sort rotation speed data timestamps
    rotation_speed_times = [dp.sample_time.ToDatetime() for dp in rotation_speed]

    # Get duration (converting it to seconds) by subtracting smallest timestamp from largest
    duration = (rotation_speed_times[-1] - rotation_speed_times[0]).total_seconds()

    # Sampling frequency
    fs = 10000  # 10000 Hz

    # Time vector
    t = np.arange(0, duration, 1 / fs)

    # Analysis data from the run
    amp = run.vibration_signal_amplitude
    phase_offset = run.vibration_signal_phase
    omega = run.average_rotation_speed

    # Create sine wave
    sine_values = amp * np.sin(omega * t + phase_offset)

    # Convert the float seconds back to timestamps
    sine_times = [rotation_speed_times[0] + timedelta(seconds=s) for s in t]

    return sine_times, sine_values


def format_number(number) -> str:
    if number == 0:
        return "0"
    digits = math.floor(math.log10(abs(number))) + 1
    precision = max(0, 3 - digits)
    format_str = "{:." + str(precision) + "f}"
    formatted_num = format_str.format(number)
    return formatted_num[:-1] if formatted_num[-2:] == '.0' else formatted_num


def format_weight(weight: FlightStand.Proto.BalanceWeight, correction_radius: float) -> str:
    if weight.unbalance != 0 and correction_radius != 0:
        unbalance = format_number(weight.unbalance * 1000.0 / correction_radius)  # Convert from kg⋅m to g⋅m
        phase_degrees = format_number(math.degrees(weight.phase))
        result = f"{unbalance} grams {phase_degrees}°"
        print(weight, " -> ", result)
        return result
    return '0 grams 0°'  # return a default string

def format_weights(weights: list[FlightStand.Proto.BalanceWeight], correction_radius: float) -> str:
    return " & ".join([format_weight(weight, correction_radius) for weight in weights])

def show_balancing_report(core: FlightStand):
    session = core.get_balancing_session()

    # Create matplotlib figure and axes
    fig, axs = plt.subplots(len(session.runs))

    #title_text = f'Session ID: {session.name}\n'
    title_text = f'Title: {session.title}\n'
    title_text += f'Motor: {session.motor_display_name}\n'
    title_text += f'Propeller: {session.propeller_display_name}\n'
    title_text += f'Operator: {session.operator_display_name}\n'
    title_text += f'Rotor mass (grams): {round(session.rotor_mass * 1000.0, 3)}\n'
    title_text += f'Operating speed (rpm): {round(session.operating_speed_rad_s * 9.5493, 3)}\n'
    title_text += f'Target quality grade (g): {round(session.quality_grade_g, 3)}\n'
    title_text += f'Permissible unbalance (g⋅m): {round(session.permissible_unbalance*1000.0, 6)}\n'
    title_text += f'Correction radius (cm): {round(session.correction_radius_m * 100.0, 3)}\n'
    title_text += f'Blades count: {session.blades_count}\n'
    #title_text += f'Control Output: {session.control_output_name}\n'
    #title_text += f'Rotation Sensor: {session.rotation_sensor_name}\n'
    #title_text += f'Vibration Sensor: {session.vibration_sensor_name}'

    # compute the overall maximum from all runs
    ymax = 0

    fig.suptitle(title_text, fontsize=8, y=0.9)
    if len(session.runs) == 1:
        axs = [axs]
    for i, (run, ax) in enumerate(zip(session.runs, axs), 1):
        run_data = core.list_balance_run_data(session.name, i-1)
        vibration_data = run_data.vibration
        rotation_speed = run_data.rotation_speed
        run_vib_max = max(dp.value for dp in vibration_data)
        if run_vib_max > ymax:
            ymax = run_vib_max
        title = "Initial run"
        if i == 2:
            title = "Trial run"
        if i > 2:
            title = f"Correction run {i - 2}/{len(session.runs) - 2}"
        tol = "in tolerance ✔"
        if session.quality_grade_g < run.quality_grade_g:
            tol = "out of tolerance ✘"
        run_details = (
            f"\n{title}\n\n"
            f"Correction weights: {format_weights(run.corrections, session.correction_radius_m)}\n"
            f"Previous corrections removed: {run.previous_weight_removed}\n"
            #f"Rotation speed (rpm): {round(run.average_rotation_speed * 9.5493, 3)}\n"
            #f"Rotation samples: {len(run.rotation_speed_data)}\n"
            f"Vibration amplitude: {round(run.vibration_signal_amplitude, 3)}\n"
            #f"Vibration phase: {round(run.vibration_signal_phase, 3)}\n"
            #f"Vibration SNR: {round(run.vibration_signal_to_noise_ratio, 3)}\n"
            #f"Vibration samples: {len(run.vibration_data)}\n"
            f"Quality grade G: {round(run.quality_grade_g, 3)} → {tol}\n"
            f"Result unbalance: {format_weight(run.unbalance, session.correction_radius_m)}\n"
            f"Next corrections: {format_weights(run.next_corrections, session.correction_radius_m)}\n"
        )
        ax.text(1.05, 0.5, run_details, transform=ax.transAxes, verticalalignment='center', fontsize=6,
                bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=1'))

        vibration_times = [dp.sample_time.ToDatetime() for dp in vibration_data]
        vibration_values = [dp.value for dp in vibration_data]
        rotation_speed_times = [dp.sample_time.ToDatetime() for dp in rotation_speed]
        sine_times, sine_values = simulate_sine_result(run, rotation_speed)

        start_time = rotation_speed_times[0]
        vibration_time_diff = [(t - start_time).total_seconds() for t in vibration_times]
        sine_time_diff = [(t - start_time).total_seconds() for t in sine_times]

        # Calculate new vibration time differences
        revolution_start_times = [(t - start_time).total_seconds() for t in rotation_speed_times]
        new_vibration_time_diff = []
        for vt in vibration_time_diff:
            revolution_start_time = max((rt for rt in revolution_start_times if rt <= vt), default=0)
            new_vibration_time_diff.append(vt - revolution_start_time)

        # Plot vibration data with updated time differences
        ax.plot(new_vibration_time_diff, vibration_values, 'o', markersize=2.0)

        # Plot the sinewave
        ax.plot(sine_time_diff, sine_values)

        # Add labels
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Vibration')

        # set the same y-axis limits for all sub-plots
        ax.set_ylim(-ymax, ymax)
        one_revolution_duration = (rotation_speed_times[1] - rotation_speed_times[0]).total_seconds()
        ax.set_xlim(0, one_revolution_duration)

    # Show plot
    plt.tight_layout()
    plt.show()
