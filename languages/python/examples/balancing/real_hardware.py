# This example interacts with the user to achieve the balancing of a propeller by asking the user a series of
# questions and asking the user to perform various actions on the hardware. Use this script in parallel to running
# the user interface to control the motor using the manual control slider.
# Note: the user interface has a balancing tab which takes care of all this sequence. This script is for illustrative
# purposes only. Also, this works with a simulated hardware too, but is less automated.
import math
import time

from examples.balancing.utils import show_balancing_report, input_float, format_weights
from flightstand import FlightStand

print("**** Running example propeller balancing script with real hardware ****")

core = FlightStand()

# Fetch the required parameters for the balancing session from the user
title = input("Enter the session title (optional, max 100 chars): ")
operator_display_name = input("Enter the operator's name (optional, max 50 chars): ")
motor_display_name = input("Enter the motor's name (optional, max 50 chars): ")
propeller_display_name = input("Enter the propeller's name (optional, max 50 chars): ")
rotor_mass_kg = input_float("Enter the rotor mass (in kg): ")
operating_speed_rpm = input_float("Enter the operating speed (rpm): ")
quality_grade_g = input_float("Enter the quality grade (optional, recommended 6.3): ")
radius_m = input_float("Enter the correction radius (m): ")
blades_count = int(
    input("Enter the number of blades for the propeller (enter zero to balance a disk without blades): "))
rotation_sensor_name = input("Enter the rotation sensor name (format '/boards/id/inputs/id'): ")
vibration_sensor_name = input("Enter the vibration sensor name (format '/boards/id/inputs/id'): ")
control_output_name = input("Enter the control output name (optional, format '/boards/id/outputs/id'): ")
control_throttle_value = input_float(f"Enter the throttle value that will spin the motor to {operating_speed_rpm} rpm. Enter 0 to control manually using the GUI: ")


def rpm_to_rad_s(rpm):
    return rpm * (2.0 * math.pi) / 60.0


# Create the balancing session with the user's parameters
core.create_balancing_session(params=core.Proto.BalancingSession(
    title=title,
    operator_display_name=operator_display_name,
    motor_display_name=motor_display_name,
    propeller_display_name=propeller_display_name,
    rotor_mass=rotor_mass_kg,
    operating_speed_rad_s=rpm_to_rad_s(operating_speed_rpm),
    quality_grade_g=quality_grade_g,
    correction_radius_m=radius_m,
    rotation_sensor_name=rotation_sensor_name,
    vibration_sensor_name=vibration_sensor_name,
    control_output_name=control_output_name,
    blades_count=blades_count
))


def grams_to_unbalance(grams):
    return grams * radius_m / 1000.0


def degrees_to_rad(degrees):
    return degrees * math.pi / 180.0


def run_motor():
    # Ask the user to bring the motor to their specified operating speed
    if control_output_name == "" or control_throttle_value == 0:
        input(f"\nPlease bring the motor to {operating_speed_rpm} rpm and press Enter")
        return
    print("Starting motor automatically")
    esc_output = core.get_output(control_output_name)
    esc_output.output_target.target_value = control_throttle_value
    core.update_output(esc_output, ['output_target'])
    time.sleep(1.0)


def stop_motor():
    if control_output_name == "":
        input("Please stop the motor and press Enter")
        return
    print("Stopping motor automatically")
    esc_output = core.get_output(control_output_name)
    esc_output.output_target.target_value = esc_output.cutoff_target.target_value
    core.update_output(esc_output, ['output_target'])


# Execute the balance run
run_motor()
core.execute_balance_run(False, None)
stop_motor()

# Retrieve the data to know the trial weight to put
session = core.get_balancing_session()
initial_run = session.runs[0]
trial_weights = initial_run.next_corrections

# Ask the user to install the calculated trial weight
print(f"\nWe recommend a trial weight of {format_weights(trial_weights, correction_radius=radius_m)}.")
actual_trial_mass_g = input_float("Enter the installed trial weight's mass (in grams): ")
actual_trial_angle = input_float("Enter the installed trial weight's angle (in degrees): ")
trial_weight = core.Proto.BalanceWeight(unbalance=grams_to_unbalance(actual_trial_mass_g),
                                        phase=degrees_to_rad(actual_trial_angle))

# Execute the trial run
print("Executing trial run")
run_motor()
core.execute_balance_run(False, [trial_weight])
stop_motor()

# Do multiple correction runs until the user decides to stop
while True:
    # Retrieve the data to know the correction weight to put
    session = core.get_balancing_session()
    last_run = session.runs[-1]
    correction_weights = last_run.next_corrections

    print(f"\nInstall correction weight(s) of {format_weights(correction_weights, correction_radius=radius_m)}")
    actual_correction_mass_g = input_float("Enter the first installed correction weight's mass (in grams): ")
    actual_correction_angle = input_float("Enter the first installed correction weight's angle (in degrees): ")
    correction_weight = core.Proto.BalanceWeight(unbalance=grams_to_unbalance(actual_correction_mass_g),
                                                 phase=degrees_to_rad(actual_correction_angle))
    actual_correction_weights = [correction_weight]

    actual_correction_mass_g = input_float(
        "Enter the second installed correction weight's mass (in grams). Write 0 if none: ")
    if actual_correction_mass_g != 0.0:
        actual_correction_angle = input_float("Enter the second installed correction weight's angle (in degrees): ")
        correction_weight = core.Proto.BalanceWeight(unbalance=grams_to_unbalance(actual_correction_mass_g),
                                                     phase=degrees_to_rad(actual_correction_angle))
        actual_correction_weights.append(correction_weight)

    # Execute the correction run
    print("Executing correction run")
    run_motor()
    core.execute_balance_run(True, actual_correction_weights)
    stop_motor()

    # Ask the user if they want to continue with more runs
    answer = input(f"Current balancing grade: {last_run.quality_grade_g}. Target grade: {session.quality_grade_g}. "
                   f"Do you want to continue with more corrections (Y/N)?:")
    if answer == "Y" or answer == "y":
        continue
    break

# Retrieve the data to print the report
show_balancing_report(core)

print("\nDone.")
