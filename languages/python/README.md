# Setup to run the example.py file
Watch video showing the step by step process to install Pycharm, the Venv, and run the example.py. Follow steps 1, 2, and 3 of part 1: [tutorial](https://youtu.be/BiZuCrVQNnU?si=9QGPBw-F-5FrZQ3p&t=68)

This example was written in [PyCharm with a venv configured](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html), using Python v3.10.
From the PyCharm terminal install the required python packages by running the command:
```bash
$ py -m pip install grpcio grpcio-tools matplotlib
```

You will also need to install the local python packages. Assuming the terminal is run from this folder, type:
```bash
$ py -m pip install -e .
```

The simulated hardware example should run normally by typing in the terminal:
```bash
$ python ./examples/example.py
```

## Troubleshooting
Some users have issues getting the above steps to work, most of them are related to the Pycharm configuration, terminal working directory, incorrectly installed Python, or improper Venv setup.
- If you get errors running the above ```$ py -m pip install -e .``` command, it is most likely because you are running the command while the terminal is in the wrong directory. It should be
  `.\flightstand-api\languages\python> py -m pip install -e .`
  and not `.flightstand-api\languages\python\examples> py -m pip install -e .
  `


- "Module 'flightstand' not found" error. Run this diagnostic command: ```$ py -c "import flightstand; print(flightstand.__file__)"```
  This command will print the directory from where the flightstand module is being imported. It should be pointing to the
  correct location of flightstand.py ("\flightstand-api\languages\python\flightstand.py
  "). If not, fix the problem by adjusting the Pycharm run configuration.


- Try different ways to launch example.py. You can use the command line like suggested in the instructions above. In Pycharm, you can also right-click on the file example.py and select "Run". If it still does not work, check the run configuration path.


- If you still get "Module 'flightstand' not found" errors, copy these five files: flightstand.py, flight_stand_api_v1_pb2.py, flight_stand_api_v1_pb2.pyi, flight_stand_api_v1_pb2_grpc.py, flight_stand_api_v1_pb2_grpc.pyi and paste them into the same folder as example.py, than try running example.py again.

# Compile Python files from the .proto
Note, the instructions below are for Tyto Robotics employees. Users of the API don't need to recompile the language files since they are already available in this project. The instructions below are to manually recompile, use our internal software publishing pipeline to automate this process.


## Setup dependencies
- Download the latest python from [python.org](python.org)
- Install the python packages, run:

```bash
$ py -m pip install grpcio grpcio-tools mypy-protobuf types-protobuf
```

If the program cannot work and print message: cannot find google module after installing grpcio and grpc-tools, try running:
```bash
$ py -m pip install google protobuf
```
## Compile proto
1) From terminal, go to root folder of this project
2) Type (note mypy helps with IDE autocomplete)
```bash
py -m grpc_tools.protoc --proto_path=proto -I. --python_out=languages/python --mypy_out=languages/python --grpc_python_out=languages/python --mypy_grpc_out=languages/python flight_stand_api_v1.proto
```