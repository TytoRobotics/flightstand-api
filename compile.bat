@ECHO OFF
Echo Compiling Python language files
python -m grpc_tools.protoc --proto_path=proto -I. --python_out=languages/python --mypy_out=languages/python --grpc_python_out=languages/python --mypy_grpc_out=languages/python flight_stand_api_v1.proto
Echo Compiling Go language files
protoc --proto_path=proto --go_out=languages/go --go-grpc_out=languages/go --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative proto/flight_stand_api_v1.proto
Echo Compiling Javascript language files
grpc_tools_node_protoc --proto_path=proto --js_out=import_style=commonjs:languages/js --grpc_out=grpc_js:languages/js flight_stand_api_v1.proto
Echo Complete!